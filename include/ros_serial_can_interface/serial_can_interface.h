/*
Copyright (c) 2016, Martin Fees (martin.fees@gmx.de)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * Neither the name of  nor the names of its contributors may be used to
   endorse or promote products derived from this software without specific
   prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file ros_serial_can_interface.h
 *
 * @brief ROS serial CAN interface - Enables communication between a CAN network and ROS wit an Arduino
 *
 * @author    Martin Fees
 * @date      25.08.2016
 * @copyright BSD 3-Clause
 *
 * This file contains all namespaces, classes, structures and enumerations for the ROS serial CAN Interface.
 * The communication between ROS and CAN is established via a Arduino UNO with a CANdiy-Shield.
 *
 */

#ifndef INCLUDE_ROS_SERIAL_CAN_INTERFACE_SERIAL_CAN_INTERFACE_H_
#define INCLUDE_ROS_SERIAL_CAN_INTERFACE_SERIAL_CAN_INTERFACE_H_

#include <ros/ros.h>
#include <memory>

#include <ros_serial_can_interface/CAN.h>
#include <ros_serial_can_interface/serial_interface.h>

/**
 * @class ROSSerialCANInterface
 *
 * @brief Manages the communication between ROS and the Arduino
 *
 *  This class manages the communication between the ROS network and
 *  the Arduino. The communication between the Arduino and the host PC is done
 *  via USB (virtual com port). To be able to communicate via the VCP the
 *  ::serialinterface namespace is used.
 */
class ROSSerialCANInterface {
 public:

  /**
   * @brief Constructor of ROSSerialCANInterface class
   *
   * @param baud[in]                       Baudrate between host PC and Arduino
   * @param serial_device_path[in]         Path to the serial device /dev/ttyACM...
   * @param update_rate_hz[in]             Update rate of the application in Hz
   * @param serial_max_timeout_time_ms[in] Maximum time to wait for data before timeout error is thrown
   *
   */
  ROSSerialCANInterface( const serialinterface::BaudTable baud,
                         const std::string& serial_device_path,
                         const double update_rate_hz,
                         const unsigned int& serial_max_timeout_time_ms);

  /**
   * @brief Destructor of ROSSerialCANInterface class
   *
   * Checks if all connections were closed by the user. If not
   * it securely closes all connections and releases the memory.
   *
   */
  ~ROSSerialCANInterface( void );

  /**
   * @brief Connects to the Arduino UNO via the serial interface
   *
   * @return True if connection was established
   */
  const bool Connect( void );

  /**
   * @brief Disconnects the Arduino UNO
   */
  void Disconnect( void );

  /**
   * @brief Executes the application and the message loop
   *
   * @return True if exited without error
   *
   * Executed the application with the configured update rate.
   * Sends data packets from the ROS network via the serial interface
   * to the Arduino. The Arduino converts the data to a CAN message and
   * sends it to the CAN network.
   * The same works vice versa for receiving data.
   */
  const bool Run ( void );

 private:

  /**
   * @brief ROS message loop is called with specified update rate in (Hz)
   *
   * @param ros_timer_event[in] Timer event
   *
   */
  void ROSMessageLoop( const ros::TimerEvent& ros_timer_event );

  /**
   * @brief Sends data from the ROS network to the Arduino
   *
   * @param tx_can_data[in] Data to send to the Arduino
   *
   */
  void TransmitCANToArduino
        ( const ros_serial_can_interface::CAN::ConstPtr& tx_can_data );

  /**
   * @brief Checks if Arduino has RX-CAN data and send it to the ROS network
   *
   */
  void ReceiveCANFromArduino( void );

  /**
   * @brief Checks the result of serialinterface::TransmitData and sends debug messages
   *
   * @param function[in] String naming the function which calls TransmitData
   * @param result[in]   Result of TransmitData
   *
   * @return true if ::ResultTable::SUCCESS, false if error
   */
  const bool CheckTransmitDataResult( const std::string& function,
                                      const serialinterface::ResultTable& result );


  /**
   * @brief Checks the result of serialinterface::ReceiveData and sends debug messages
   *
   * @param function[in] String naming the function which calls TransmitData
   * @param result[in]   Result of TransmitData
   *
   * @return true if ::ResultTable::SUCCESS, false if error
   */
  const bool CheckReceiveDataResult( const std::string& function,
                                     const serialinterface::ResultTable& result );

  ros::NodeHandle   ros_node_handle_;   //!< ROS node handle to access ROS objects
  ros::Timer        ros_update_timer_;  //!< ROS update timer for message loop

  ros::Subscriber   ros_can_tx_subscriber_;  //!< ROS Subscriber receiving CAN messages from ROS
  ros::Publisher    ros_can_rx_publisher_;   //!< ROS Publisher sending received CAN messages to ROS

  std::unique_ptr<serialinterface::Manager> serial_interface_;  //!< Serial interface between PC and Arduino
};

#endif /*INCLUDE_ROS_SERIAL_CAN_INTERFACE_SERIAL_CAN_INTERFACE_H_*/
