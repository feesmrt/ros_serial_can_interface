/*
Copyright (c) 2016, Martin Fees (martin.fees@gmx.de)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * Neither the name of  nor the names of its contributors may be used to
   endorse or promote products derived from this software without specific
   prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file serial_interface.h
 *
 * @brief Serial interface managing communication between host PC and serial devices
 *
 * @author    Martin Fees
 * @date      16.08.2016
 * @copyright BSD 3-Clause
 *
 * Namespace which contains all class needed to communicate via a serial interface (e.g. VCP, RS232).
 *
 */

#ifndef INCLUDE_ROS_SERIAL_CAN_INTERFACE_SERIAL_INTERFACE_H_
#define INCLUDE_ROS_SERIAL_CAN_INTERFACE_SERIAL_INTERFACE_H_

#include <iostream>
#include <memory>
#include <boost/asio.hpp>
#include <boost/system/system_error.hpp>
#include <boost/bind.hpp>

/**
 * @namespace   serialinterface
 *
 * @brief       Namespace containing serial interface objects
 *
 * This namespace contains classes, enumerations and
 * structures which are managing the serial communication
 */
namespace serialinterface
{

/**
 * @enum DefinitionTable
 *
 * @brief Definitions for ::serialinterface namespace
 *
 * Enumeration containing all definitions needed in the ::serialinterface
 * namespace. The enumeration is used instead of the #define compiler flag.
 */
enum DefinitionTable {
  MAXIMUM_ERROR_DISCONNECT  = 3,    //!< Maximum error allowed before disconnecting device
  MAXIMUM_DATA_BUFFER_SIZE  = 128,  //!< Maximum data size of a 8 bit buffer
};

/**
 * @enum ResultTable
 *
 * @brief Table with possible results
 *
 * Enumeration containing the possible results returned by the functions
 * of the ::Manager class.
 */
enum ResultTable {
  ERROR                          = 0, //!< Function throws general error
  SUCCESS                        = 1, //!< Function was successfully executed
  ERROR_ALREADY_CONNECTED        = 2, //!< Connection is already established
  ERROR_DEVICE_NOT_FOUND         = 3, //!< Device not found (no such file or directory)
  ERROR_DEVICE_PERMISSION_DENIED = 4, //!< Permission to access device is denied
  ERROR_OPERATION_PERMITTED      = 5, //!< Opening device is permitted
  ERROR_DEVICE_INVALID_BAUD      = 6, //!< Invalid baud was set (not supported)
  ERROR_DEVICE_INVALID_DATA_BITS = 7, //!< Invalid data bits per frame was set (not supported)
  ERROR_DEVICE_INVALID_FLOW_CTRL = 8, //!< Invalid flow ctrl was set (not supported)
  ERROR_DEVICE_INVALID_STOP_BITS = 9, //!< Invalid stop bits was set (not supported)
  ERROR_DEVICE_INVALID_PARITY    = 10,//!< Invalid parity option was set (not supported)
  ERROR_NOT_CONNECTED            = 12,//!< Function failed: Connect() was not called
  ERROR_TX                       = 13,//!< Failed to transmit data to device
  ERROR_RX                       = 14,//!< Failed to receive data from device
  ERROR_RX_TIMEOUT               = 15,//!< Failed to receive data from device due to timeout
  ERROR_MAXIMUM_BUFFER_SIZE      = 16,//!< Maximum buffer size reached (see ::DefinitionTable)
};

/**
 * @enum BaudTable
 *
 * @brief Table with baud rates
 *
 * Enumeration containing the baud rates, which can be used for the interface.
 * The baud rates are inspired from ARDUINO VCP.
 */
enum BaudTable {
  BAUD_300    = 300,    //!< Baud 300 bps
  BAUD_600    = 600,    //!< Baud 600 bps
  BAUD_1200   = 1200,   //!< Baud 1200 bps
  BAUD_2400   = 2400,   //!< Baud 2400 bps
  BAUD_4800   = 4800,   //!< Baud 4800 bps
  BAUD_9600   = 9600,   //!< Baud 9600 bps
  BAUD_14400  = 14400,  //!< Baud 14400 bps
  BAUD_19200  = 19200,  //!< Baud 19200 bps
  BAUD_28800  = 28800,  //!< Baud 28800 bps
  BAUD_31250  = 31250,  //!< Baud 31250 bps
  BAUD_38400  = 38400,  //!< Baud 38400 bps
  BAUD_57600  = 57600,  //!< Baud 57600 bps
  BAUD_115200 = 115200, //!< Baud 115200 bps
  BAUD_128000 = 128000  //!< Baud 128000 bps
};

/**
 * @enum DataBitsTable
 *
 * @brief Table with data bits
 *
 * Enumeration containing table with possible data bit length of
 * a message.
 */
enum DataBitsTable {
  DATA_5 = 5, //!< 5 bits for data transport
  DATA_6 = 6, //!< 6 bits for data transport
  DATA_7 = 7, //!< 7 bits for data transport
  DATA_8 = 8  //!< 8 bits for data transport
};

/**
 * @enum ParityTable
 *
 * @brief Table with parity options
 *
 * Enumeration containing table with possible parity settings.
 */
enum ParityTable {
  NONE  = 0,  //!< No parity bit is send
  ODD   = 1,  //!< Odd parity bit is send
  EVEN  = 2   //!< Even parity bit is send
};

/**
 * @enum StopBitTable
 *
 * @brief Table with stop bit options
 *
 * Enumeration of possible stop bits.
 */
enum StopBitTable {
  STOP_1    = 1,  //!< One stop bit is send
  STOP_2    = 2,  //!< Two stop bits are send
  STOP_1_5  = 3   //!< 1.5 stop bits are send
};

/**
 * @class Manager
 *
 * @brief Manages communication via serial interface
 *
 * This class manages the communication via serial interface.
 * It initializes the required boost components and manages the communication.
 *
 */
class Manager {
public:
  /**
   * @brief Constructor of Manager class
   *
   * @param device_path[in] Absolute path to the device
   * @param baud[in]        Communication speed in baud per second (bps)
   * @param data_bits[in]   Number of data bits per frame
   * @param parity[in]      Parity bit of frame
   * @param stop_bits[in]   Stop bits per frame
   * @param timeout_max[in] Maximum timeout time in milliseconds
   *
   */
  Manager( const std::string& device_path, const BaudTable& baud,
           const DataBitsTable& data_bits, const ParityTable& parity,
           const StopBitTable& stop_bits, const unsigned int timeout_max );

  /**
   * @brief Destructor of Manager class
   *
   * Checks if the connection was closed by user. If not it closes
   * the connection and releases the allocated memory.
   *
   */
  ~Manager( void );

  /**
   * @brief Connects to the device
   *
   * Connects to the specified device (device_path) with the desired
   * configuration set via the constructor.
   *
   * @return see ::ResultTable
   */
  const ResultTable Connect( void );
  
  /**
   * @brief Closes the connection to the device
   *
   * Closes the connection to the specified device and releases
   * the allocated memory.
   */
  void Disconnect( void );
  
  /**
   * @brief Transmits a data buffer
   * 
   * Transmits a data array (tx_data) of size (tx_data_size) via
   * the connected serial interface.
   *
   * @param tx_data_buffer[in] Array of data to transmit
   * @param tx_data_size[in] Size of the array
   *
   * @return ( see ::ResultTable )
   */
  const ResultTable TransmitData( const unsigned char tx_data_buffer[],
                                  const unsigned int tx_data_size);

  /**
  * @brief Receives data (blocking) 
  *
  * Receives data via the serial interface. This function blocks the
  * execution until receiving is completed or timeout error occurred.
  * To stop receiving the device has to send the escape sequence to
  * the host PC (\\r\\n).
  * 
  * @param rx_data_buffer[out] Reference to vector containing the received data
  *
  * @return ( see ::ResultTable )
  */
  const ResultTable ReceiveData( std::vector<unsigned char>& rx_data_buffer );

  /**
   * @return String with device path
   */
  const std::string&  get_device_path( void );

  /**
   * @return Baud rate in (bps) of connection (see ::BaudTable)
   */
  const BaudTable get_baud( void );

  /**
   * @return Data bits send per frame (see ::DataBitsTable)
   */
  const DataBitsTable get_data_bits( void );

  /**
   * @return Parity used for frame check (see ::ParityTable)
   */
  const ParityTable get_parity( void );

  /**
   * @return Stop bits used per frame (see ::StopBitTable)
   */
  const StopBitTable get_stop_bits( void );

  /**
   * @return Maximum timeout time in (milliseconds) before error
   */
  const double get_timeout_max( void );

  /**
   * @return Number of errors occured since last success
   */
  const unsigned int get_error_counter( void );

  /**
   * @return True if a connection is established
   */
  const bool is_connected( void );

private:

  /**
   * @brief Receive one byte from boost::asio
   *
   * Receives one byte ( unsigned char ) from device. This function blocks
   * the program until a byte is received or an error is thrown.
   * Errors could be ERROR_TIMEOUT or general ERROR_RX
   *
   * @param rx_data[out] Reference to data
   *
   * @return ( see ::ResultTable )
   */
  const ResultTable ReceiveByte( unsigned char& rx_data );

  /**
   * @brief Callback function for reading data with async_read_some
   *
   * @param data_available[out] True if data is available
   * @param timer_timeout[in] Reference to timeout timer
   * @param boost_error_code[out] Error code
   * @param num_bytes_transferred[out] Number of bytes transferred
   */
  void ReadCallback( bool& data_available,
                     boost::asio::deadline_timer& timer_timeout,
                     const boost::system::error_code& boost_error_code,
                     std::size_t num_bytes_transferred );

  /**
   * @brief Callback for timer
   *
   * @param serial_port[in] Reference to the serial port
   * @param boost_error_code[in] Error code
   * @param rx_cancelled[out] True if rx was cancelled
   */
  void WaitCallback( boost::asio::serial_port& serial_port,
                     const boost::system::error_code& boost_error_code,
                     bool& rx_cancelled );

  /**
   * @brief Checks number of detected errors and disconnects if to many errors occured
   *
   * @param result[in] Result from ::ResultTable
   *
   * @return result[in]
   */
  const ResultTable ResultTableManager(const ResultTable& result);

  const std::string           device_path_;   //!< Absolute path to the serial device
  const BaudTable             baud_;          //!< Communication speed in baud per second (bps)
  const DataBitsTable         data_bits_;     //!< Number of data bits per frame
  const ParityTable           parity_;        //!< Parity bit of frame
  const StopBitTable          stop_bits_;     //!< Stop bit setting of frame
  const unsigned int          timeout_max_;   //!< Saves the maximum timeout time (milliseconds)

  std::unique_ptr<boost::asio::io_service>      io_service_;    //!< I/O service from boost library
  std::unique_ptr<boost::asio::serial_port>     serial_port_;   //!< Instance of serial port from boost library
  std::unique_ptr<boost::asio::deadline_timer>  timer_timeout_; //!< Timer for checking timeout of RX event from boost library

  unsigned int    error_counter_; //!< Counts the number of errors occured

  bool                          is_mem_allocated_; //!< Checks if the memory is already allocated
  bool                          is_connected_;     //!< Checks if connection is already established
};
};

#endif /* INCLUDE_ROS_SERIAL_CAN_INTERFACE_SERIAL_INTERFACE_H_ */
