/*
Copyright (c) 2016, Martin Fees (martin.fees@gmx.de)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * Neither the name of  nor the names of its contributors may be used to
   endorse or promote products derived from this software without specific
   prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file ros_serial_can_interface.cc
 *
 * @brief Source code of ROS Serial CAN Interface
 *
 * @author    Martin Fees
 * @date      25.08.2016
 * @copyright BSD 3-Clause
 */

#include <ros_serial_can_interface/serial_can_interface.h>

ROSSerialCANInterface::ROSSerialCANInterface(
                 const serialinterface::BaudTable baud,
                 const std::string& serial_device_path,
                 const double update_rate_hz,
                 const unsigned int& serial_max_timeout_time_ms )
: ros_node_handle_( ),
  ros_update_timer_( ),
  ros_can_tx_subscriber_( ),
  ros_can_rx_publisher_( ),
  serial_interface_( ) {

 // register update timer for CAN RX event
 ros_update_timer_ = ros_node_handle_.createTimer
                       ( ros::Duration( 1.0 / update_rate_hz ),
                         &ROSSerialCANInterface::ROSMessageLoop,
                         this );


 // register subscriber for can messages ( ROS -> Arduino )
 ros_can_tx_subscriber_ =
   ros_node_handle_.subscribe
     <ros_serial_can_interface::CAN>
       ("CAN/tx",
        100,
        &ROSSerialCANInterface::TransmitCANToArduino,
        this
       );

 // register publisher for can messages ( Arduino -> ROS )
 ros_can_rx_publisher_  =
   ros_node_handle_.advertise<ros_serial_can_interface::CAN>( "CAN/rx", 100 );

 // create serial interface to Arduino
 if( !serial_interface_ ) {
   serial_interface_ =
       std::unique_ptr<serialinterface::Manager>
         ( new serialinterface::Manager( serial_device_path,
                                         baud,
                                         serialinterface::DATA_8,
                                         serialinterface::NONE,
                                         serialinterface::STOP_1,
                                         serial_max_timeout_time_ms ) );
 }
}

ROSSerialCANInterface::~ROSSerialCANInterface( void ) {
  // Disconnect
  Disconnect( );

}

const bool ROSSerialCANInterface::Connect( void ) {

  // variables
  serialinterface::ResultTable result = serialinterface::SUCCESS;

  ROS_INFO( "ros_serial_can_interface: Connecting to ARDUINO... " );

  // wait
  sleep ( 1 );

  // connect to arduino
  result = ( *serial_interface_ ).Connect( );

  // check result
  switch( result ) {
    case serialinterface::SUCCESS: {
      // connection successfully established
      break;
    }

    case serialinterface::ERROR: {
      ROS_ERROR( "ros_serial_can_interface: Failed to connect to ARDUINO: "
                 "general error!" );
      (* serial_interface_ ).Disconnect( );
      return false;
    }

    case serialinterface::ERROR_ALREADY_CONNECTED: {
      ROS_ERROR( "ros_serial_can_interface: Failed to connect to ARDUINO: "
                 "ARDUINO is already connected!" );
      (* serial_interface_ ).Disconnect( );
      return false;
    }

    case serialinterface::ERROR_DEVICE_NOT_FOUND: {
      ROS_ERROR( "ros_serial_can_interface: Failed to connect to ARDUINO: "
                 "Device is not found! Is the ARDUINO connected to the PC?" );
      (* serial_interface_ ).Disconnect( );
      return false;
    }

    case serialinterface::ERROR_DEVICE_PERMISSION_DENIED: {
      ROS_ERROR( "ros_serial_can_interface: Failed to connect to ARDUINO: "
                 "Permission denied! Please check access rights! "
                 "Call sudo chown user device_path" );
      (* serial_interface_ ).Disconnect( );
      return false;
    }

    case serialinterface::ERROR_OPERATION_PERMITTED: {
      ROS_ERROR( "ros_serial_can_interface: Failed to connect to ARDUINO: "
                 "Operation permitted!" );
      (* serial_interface_ ).Disconnect( );
      return false;
    }

    case serialinterface::ERROR_DEVICE_INVALID_BAUD: {
      ROS_ERROR( "ros_serial_can_interface: Failed to connect to ARDUINO: "
                 "Baudrate %i is not supported!",
                 ( int )( serial_interface_->get_baud() ) );
      (* serial_interface_ ).Disconnect( );
      return false;
    }

    case serialinterface::ERROR_DEVICE_INVALID_DATA_BITS: {
      ROS_ERROR( "ros_serial_can_interface: Failed to connect to ARDUINO: "
                 "Data bit configuration is not supported!" );
      (* serial_interface_ ).Disconnect( );
      return false;
    }

    case serialinterface::ERROR_DEVICE_INVALID_FLOW_CTRL: {
      ROS_ERROR( "ros_serial_can_interface: Failed to connect to ARDUINO: "
                 "Flow control configuration is not supported!" );
      (* serial_interface_ ).Disconnect( );
      return false;
    }

    case serialinterface::ERROR_DEVICE_INVALID_STOP_BITS: {
      ROS_ERROR( "ros_serial_can_interface: Failed to connect to ARDUINO: "
                 "Stop bit configuration is not supported!" );
      (* serial_interface_ ).Disconnect( );
      return false;
    }

    case serialinterface::ERROR_DEVICE_INVALID_PARITY: {
      ROS_ERROR( "ros_serial_can_interface: Failed to connect to ARDUINO: "
                 "Parity configuration is not supported!" );
      (* serial_interface_ ).Disconnect( );
      return false;
    }

    default: {
      ROS_ERROR( "ros_serial_can_interface: Failed to connect to ARDUINO: "
                 "Unknown error!" );
      (* serial_interface_ ).Disconnect( );
      return false;
    }

  };

  ROS_INFO( "ros_serial_can_interface: Cleanup serial interface..." );

  // wait 2 second
  sleep( 2 );

  // flush invalid data
  const unsigned char flush_data[4] = { 0x00, 0x00, '\r', '\n' };

  // transmit sequence to flush invalid data
  if( serialinterface::SUCCESS !=
      (* serial_interface_).TransmitData( flush_data, 4 )) {
    ROS_ERROR( "ros_serial_can_interface: Failed to cleanup serial interface!" );
    (* serial_interface_ ).Disconnect( );
    return false;
  }

  // wait
  sleep ( 2 );

  ROS_INFO( "ros_serial_can_interface: Cleanup serial interface... Done" );
  ROS_INFO( "ros_serial_can_interface: Connecting to ARDUINO... Done!" );

 return true;
}

void ROSSerialCANInterface::Disconnect( void ) {
  // stop timer
  ros_update_timer_.stop( );

  // close connection
  ( *serial_interface_ ).Disconnect( );

  // clean up
  if( serial_interface_ ) {
      serial_interface_.reset();
    }
}

const bool ROSSerialCANInterface::Run( void ) {
  // check if serial interface is connected
  if( false == ( *serial_interface_ ).is_connected() ) {
    ROS_ERROR("ros_serial_can_interface: ARDUINO is not connected!");
    return false;
  }

  // run application
  ros::spin();

 return true;
}

void ROSSerialCANInterface::ROSMessageLoop
      ( const ros::TimerEvent& ros_timer_event )  {

  // check if device is connected
  if( false == ( *serial_interface_ ).is_connected() ) {
    // connection lost -> try to reconnect
    ROS_WARN( "ros_serial_can_interface: Connection lost! Try to reconnect..." );

    // reconnect
    Connect();

    return;
  }

  // receive data from ARDUINO
  ReceiveCANFromArduino( );

}

void ROSSerialCANInterface::TransmitCANToArduino
      ( const ros_serial_can_interface::CAN::ConstPtr& tx_can_data ) {
  // variables
  serialinterface::ResultTable  result = serialinterface::SUCCESS;
  unsigned char tx_data_buffer[14];
  unsigned char tx_data_buffer_size = 0;

  // create header: ( CMD | CAN-ID (High) | CAN-ID (Low) )
  tx_data_buffer[0] = 0x11; // TX command

  // save CAN-ID
  tx_data_buffer[1] = ( unsigned char )( tx_can_data->ID >> 8 );
  tx_data_buffer[2] = ( unsigned char )( tx_can_data->ID );
  tx_data_buffer_size = 3;

  // copy data to tx buffer
  for( unsigned char n = 0; n < tx_can_data->data_size; n++ ) {
    tx_data_buffer[tx_data_buffer_size] = tx_can_data->data[n];
    tx_data_buffer_size++;
  }

  // write escape sequence
  tx_data_buffer[tx_data_buffer_size] = '\r';
  tx_data_buffer_size++;

  tx_data_buffer[tx_data_buffer_size] = '\n';
  tx_data_buffer_size++;

  // transmit message
  result  = ( *serial_interface_ ).TransmitData( tx_data_buffer,
                                                 tx_data_buffer_size );

  // check transmit result
  CheckTransmitDataResult( "TransmitCANToArduino", result );
}

void ROSSerialCANInterface::ReceiveCANFromArduino( void ) {

  // variables
  static ros_serial_can_interface::CAN can_msg;
  serialinterface::ResultTable  result = serialinterface::SUCCESS;
  unsigned char tx_data_buffer[3];
  unsigned char tx_data_buffer_size = 0;
  std::vector<unsigned char> rx_data_buffer;

  // send command to read RX buffer of ARDUINO
  tx_data_buffer[0] = 0x44; //CMD to read bufer

  // escape sequence
  tx_data_buffer[1] = '\r';
  tx_data_buffer[2] = '\n';

  // 3 bytes of data
  tx_data_buffer_size = 3;

  // send request to download RX buffer from arduino
  result = ( *serial_interface_ ).TransmitData( tx_data_buffer,
                                                tx_data_buffer_size );

  // check result
  if( !CheckTransmitDataResult( "ReceiveCANFromArduino", result ) ) {
    // Error occured abort
    return;
  }

  // wait for data to receive
  result = ( *serial_interface_ ).ReceiveData( rx_data_buffer );

  // check receive result
  if( !CheckReceiveDataResult( "ReceiveCANFromArduino", result) ) {
    // error occured abort
    return;
  }

  // check if CANdiy shield can't be initialized by Arduino
  if( ( rx_data_buffer.size() == 1 ) && ( rx_data_buffer[0] == 'e' ) ) {
    ROS_ERROR( "ros_serial_can_interface: CANdiy-Shield initialisation failed! "
               "Check connection between Arduino UNO and CANdiy-Shield" );

    sleep( 1 );

    return;
  }

  // check if there is data in the buffer
  else if( !( rx_data_buffer.size() == 1 && rx_data_buffer[0] == 0xff) ) {
    // copy received data to CAN message

    // copy ID
    can_msg.ID  = (uint16_t)(rx_data_buffer[0] << 8 | rx_data_buffer[1]);

    // reset
    can_msg.data_size = 0;

    // check for overflow
    if( 10 < rx_data_buffer.size() ) {
      ROS_WARN( "ros_ros_serial_can_interface: ReceiveCANFromArduino: "
                "Received data is to long for CAN message!" );
      return;
    }

    // copy data
    for( unsigned int n = 0; n < sizeof(can_msg.data); n++ ) {
      // check if there is data to fill in
      if( n < (rx_data_buffer.size() - 2) ) {
        can_msg.data[ n ] = rx_data_buffer[ n + 2 ];
        can_msg.data_size ++;
      }
      else {
        // fill with no data
        can_msg.data[ n ] = 0x00;
      }
    }

    // setup header
    can_msg.header.stamp  = ros::Time::now();
    can_msg.header.frame_id = "ros_serial_can_interface";

    // push
    ros_can_rx_publisher_.publish( can_msg );
  }

}

const bool ROSSerialCANInterface::CheckTransmitDataResult
            ( const std::string& function,
              const serialinterface::ResultTable& result ) {

  // check result
  switch( result ) {
    case serialinterface::SUCCESS: {
      return true;
    }

    case serialinterface::ERROR_NOT_CONNECTED: {
      ROS_WARN( "ros_serial_can_interface: %s: "
                "Device is not connected!",
                function.c_str() );
      return false;
    }

    case serialinterface::ERROR_TX: {
      ROS_WARN( "ros_serial_can_interface: %s: "
                "Failed to transmit data!",
                function.c_str() );
      return false;
    }

    default: {
      ROS_WARN( "ros_serial_can_interface: %s: "
                "Unknown error detected!",
                function.c_str() );

      return false;
    }
  };

  return false;
}

const bool ROSSerialCANInterface::CheckReceiveDataResult
            ( const std::string& function,
              const serialinterface::ResultTable& result ) {

  // check result
  switch( result ) {
    case serialinterface::SUCCESS: {
      return true;
    }

    case serialinterface::ERROR_NOT_CONNECTED: {
      ROS_WARN( "ros_serial_can_interface: %s: "
                "Device is not connected!",
                function.c_str() );
      return false;
    }

    case serialinterface::ERROR_RX: {
      ROS_WARN( "ros_serial_can_interface: %s: "
                "Failed to receive data!",
                function.c_str() );
      return false;
    }

    case serialinterface::ERROR_MAXIMUM_BUFFER_SIZE: {
      ROS_WARN( "ros_serial_can_interface: %s: "
                "Maximum RX buffer size is reached!",
                function.c_str() );
      return false;
    }

    case serialinterface::ERROR_RX_TIMEOUT: {
      ROS_WARN( "ros_serial_can_interface: %s: "
                "Timeout time has passed!",
                function.c_str() );
      return false;
    }

    default: {
      ROS_WARN( "ros_serial_can_interface: %s: "
                "Unknown error detected!",
                function.c_str() );

      return false;
    }
  };


  return false;
}

bool InitApplication( ros::NodeHandle& ros_node_handle,
                      std::unique_ptr<ROSSerialCANInterface>& can_interface) {

  // variables for parameters
  std::string serial_device_path;
  double update_rate_hz = 0.0;
  int timeout_ms = 0;
  int baud = 9600;

  // get parameters from param server

  // get baud of serial device
  ros_node_handle.param<int>( "baud",
                              baud,
                              9600 );

  // get path to serial device
  ros_node_handle.param<std::string>( "serialDevicePath",
                                      serial_device_path,
                                      "/dev/ttyACM0" );

  // get update rate in Hz
  ros_node_handle.param<double>( "updateRateHz",
                                  update_rate_hz,
                                  100.0 );

  // get maximum time before timeout error is raised
  ros_node_handle.param<int>( "timeout",
                              timeout_ms,
                              100 );

  serialinterface::BaudTable baud_table = serialinterface::BaudTable::BAUD_9600;

    // convert baud
    switch( baud ) {
      case 300:  {
        baud_table = serialinterface::BaudTable::BAUD_300;
        break;
      }
      case 600:  {
        baud_table = serialinterface::BaudTable::BAUD_600;
        break;
      }
      case 1200:  {
        baud_table = serialinterface::BaudTable::BAUD_1200;
        break;
      }
      case 2400:  {
        baud_table = serialinterface::BaudTable::BAUD_2400;
        break;
      }
      case 4800:  {
        baud_table = serialinterface::BaudTable::BAUD_4800;
        break;
      }
      case 9600:  {
        baud_table = serialinterface::BaudTable::BAUD_9600;
        break;
      }
      case 14400:  {
        baud_table = serialinterface::BaudTable::BAUD_14400;
        break;
      }
      case 19200:  {
        baud_table = serialinterface::BaudTable::BAUD_19200;
        break;
      }
      case 28800:  {
        baud_table = serialinterface::BaudTable::BAUD_28800;
        break;
      }
      case 31250:  {
        baud_table = serialinterface::BaudTable::BAUD_31250;
        break;
      }
      case 38400:  {
        baud_table = serialinterface::BaudTable::BAUD_38400;
        break;
      }
      case 57600:  {
        baud_table = serialinterface::BaudTable::BAUD_57600;
        break;
      }
      case 115200:  {
        baud_table = serialinterface::BaudTable::BAUD_115200;
        break;
      }
      case 128000:  {
        baud_table = serialinterface::BaudTable::BAUD_128000;
        break;
      }
      default: {
        // invalid baud rate was set (not supported by device)
        ROS_ERROR( "ros_serial_can_interface: Baud %i bps not supported! ",
                   baud );
        return false;
      }
    };

  // create arduino can interface instance
  if( !can_interface ) {
    can_interface =
        std::unique_ptr<ROSSerialCANInterface>
          ( new ROSSerialCANInterface( baud_table,
                                       serial_device_path,
                                       update_rate_hz,
                                       timeout_ms) );
  }

  return true;
}

void SigintHandler(int sig)
{
  ROS_INFO( "ros_serial_can_interface: Running ros_serial_can_interface node... Done!" );

  //shutdown
  ros::shutdown();
}

int main (int argc, char** argv) {
  // variables
  std::unique_ptr<ROSSerialCANInterface> ros_serial_can_interface;

  // initialize ROS
  ros::init(argc, argv, "ros_serial_can_interface");

  // get ROS node handle
  ros::NodeHandle ros_node_handle("~");

  // SIGINT handler
  signal(SIGINT, SigintHandler);

  ROS_INFO( "ros_serial_can_interface: Starting application... " );

  // Initialize application
  if( !InitApplication( ros_node_handle, ros_serial_can_interface ) ) {
    ROS_ERROR("ros_serial_can_interface: Failed to init application!");
    return -1;
  }

  ROS_INFO( "ros_serial_can_interface: Starting application... Done!" );


  // Connect to Arduino
  if( !( *ros_serial_can_interface ).Connect( ) ) {
    ROS_ERROR("ros_serial_can_interface: Failed to connect to ARDUINO!");
    return -2;
  }

  ROS_INFO( "ros_serial_can_interface: Running ros_serial_can_interface node... " );

  // Run application
  ( *ros_serial_can_interface ).Run( );


  return 0;
}
