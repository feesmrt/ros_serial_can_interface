/*
Copyright (c) 2016, Martin Fees (martin.fees@gmx.de)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * Neither the name of  nor the names of its contributors may be used to
   endorse or promote products derived from this software without specific
   prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file can_talker.cc
 *
 * @brief Application which sends random CAN messages
 *
 * @author    Martin Fees
 * @date      27.08.2016
 * @copyright BSD 3-Clause
 */

#include <ros/ros.h>

#include <ros_serial_can_interface/CAN.h>
#include <ros_serial_can_interface/serial_interface.h>

/**
 * @brief Main function of CAN talker
 *
 * @param argc
 * @param argv
 *
 * This functions is used to test the CAN interface.
 * It sends CAN messages to the ARDUINO which publishs it to the CAN network.
 *
 * @return
 */
int main (int argc, char** argv) {

  // initialize ROS
  ros::init(argc, argv, "can_talker");

  // get ROS node handle
  ros::NodeHandle ros_node_handle("~");

  ros::Publisher  ros_can_publisher;
  ros_can_publisher = ros_node_handle.advertise
                        <ros_serial_can_interface::CAN>("/CAN/tx", 10);

  // message loop
  ros::Rate rate( 0.5 );
  uint8_t data_cnt = 0;
  while( ros::ok() ) {
    // create message
    ros_serial_can_interface::CAN can_msg;

    can_msg.header.stamp  = ros::Time::now();
    can_msg.header.frame_id = "can_talker";

    can_msg.ID  = 123;

    can_msg.data[0] = data_cnt++;
    can_msg.data[1] = data_cnt++;
    can_msg.data[2] = data_cnt++;
    can_msg.data[3] = data_cnt++;
    can_msg.data[4] = data_cnt++;
    can_msg.data[5] = data_cnt++;
    can_msg.data[6] = data_cnt++;
    can_msg.data[7] = data_cnt++;
    can_msg.data_size = 8;

    ros_can_publisher.publish( can_msg );

    rate.sleep();
  }


  return 0;
}
