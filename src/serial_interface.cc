/*
Copyright (c) 2016, Martin Fees (martin.fees@gmx.de)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * Neither the name of  nor the names of its contributors may be used to
   endorse or promote products derived from this software without specific
   prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file serial_interface.cc
 *
 * @brief Source code of serial interface namespace
 *
 * @author    Martin Fees
 * @date      16.08.2016
 * @copyright BSD 3-Clause
 */

#include <ros_serial_can_interface/serial_interface.h>

namespace serialinterface {
Manager::Manager(const std::string& device_path, const BaudTable& baud,
                 const DataBitsTable& data_bits, const ParityTable& parity,
                 const StopBitTable& stop_bits, const unsigned int timeout_max)
  : device_path_( device_path ),
    baud_( baud ),
    data_bits_( data_bits ),
    parity_( parity ),
    stop_bits_( stop_bits ),
    timeout_max_( timeout_max ),
    io_service_(),
    serial_port_(),
    timer_timeout_(),
    error_counter_(0),
    is_mem_allocated_( false ),
    is_connected_( false ) {

}

Manager::~Manager( void ) {
  Disconnect();
}

const ResultTable Manager::Connect( void ) {
  // check if connection was alread established
  if( true == is_connected_ ) {
    // function is already initialized
    return ResultTable::ERROR_ALREADY_CONNECTED;
  }

  // check if memory has to be allocated
  if( false == is_mem_allocated_ ) {
    io_service_    =
      std::unique_ptr<boost::asio::io_service>
      ( new boost::asio::io_service );

    serial_port_   =
        std::unique_ptr<boost::asio::serial_port>
        ( new boost::asio::serial_port( *io_service_ ) );

    timer_timeout_ =
        std::unique_ptr<boost::asio::deadline_timer>
        ( new boost::asio::deadline_timer( *io_service_ ) );

    //memory is allocated
    is_mem_allocated_ = true;
  }

  // try to open device
  try {
    ( *serial_port_ ).open( device_path_ );
  }
  // catch errors
  catch( boost::system::system_error& boost_error ) {
    // check error code
    switch ( boost_error.code().value() ) {
      case 1:{
        // Opening device was permitted
        return ResultTable::ERROR_OPERATION_PERMITTED;
        break;
      }
      case 2:{
        // Device was not found -> maybe wrong path defined
        return ResultTable::ERROR_DEVICE_NOT_FOUND;
        break;
      }
      case 13: {
        // No permission to open device -> check user access rights
        return ResultTable::ERROR_DEVICE_PERMISSION_DENIED;
        break;
      }
      default: {
        // General error
        return ResultTable::ERROR;
        break;
      }
    }
  }

  // device is connected
  is_connected_ = true;

  // convert baudrate
  unsigned int baud = static_cast<unsigned int>(baud_);

  // try to set baud
  try {
   ( *serial_port_ ).set_option(
       boost::asio::serial_port_base::baud_rate( baud ));
  }
  // catch errors
  catch( boost::system::system_error& boost_error ) {
    // check error code
    if ( boost_error.code().value() == 22 ) {
      // invalid baud rate was set (not supported by device)
      Disconnect();
      return ResultTable::ERROR_DEVICE_INVALID_BAUD;

    }
  }

  // try to set flow control
  try {
    // set flow control to none
    boost::asio::serial_port_base::flow_control flwctrl(
        boost::asio::serial_port_base::flow_control::none );

    // apply setting
    ( *serial_port_ ).set_option( flwctrl );
  }
  // catch errors
  catch( boost::system::system_error& boost_error ) {
    // error
    Disconnect();
    return ResultTable::ERROR_DEVICE_INVALID_FLOW_CTRL;
  }

  // try to set data bits per frame
  try {
    switch( data_bits_ ) {
      case DATA_5: {
        ( *serial_port_ ).set_option(
            boost::asio::serial_port_base::character_size( 5 ) );

        // NOT SUPPORTED YET
        Disconnect();
        return ResultTable::ERROR_DEVICE_INVALID_DATA_BITS;
        break;
      }  // check if data was received  // check if data was received


      case DATA_6: {
        ( *serial_port_ ).set_option(
            boost::asio::serial_port_base::character_size( 6 ) );

        // NOT SUPPORTED YET
        Disconnect();
        return ResultTable::ERROR_DEVICE_INVALID_DATA_BITS;
        break;
      }
      case DATA_7:  {
        (*serial_port_).set_option(
            boost::asio::serial_port_base::character_size( 7 ) );

        // NOT SUPPORTED YET
        Disconnect();
        return ResultTable::ERROR_DEVICE_INVALID_DATA_BITS;
        break;
      }
      case DATA_8:  {
        (*serial_port_).set_option(
            boost::asio::serial_port_base::character_size( 8 ) );
        break;
      }
    }
  }
  catch ( boost::system::system_error& boost_error ) {
      // error
      Disconnect();
      return ResultTable::ERROR_DEVICE_INVALID_DATA_BITS;
    }

  // try to set stop bits
  try {
    switch( stop_bits_ ) {
      case STOP_1: {
        boost::asio::serial_port_base::stop_bits stop_bits(
            boost::asio::serial_port_base::stop_bits::one );

        // set stop bit
        ( *serial_port_ ).set_option(stop_bits);

        break;
      }
      case STOP_2: {
        boost::asio::serial_port_base::stop_bits stop_bits(
            boost::asio::serial_port_base::stop_bits::two );

        // set stop bit
        (*serial_port_).set_option(stop_bits);

        break;
      }
      case STOP_1_5: {
        boost::asio::serial_port_base::stop_bits stop_bits(
            boost::asio::serial_port_base::stop_bits::onepointfive );

        // set stop bit
        ( *serial_port_ ).set_option( stop_bits );

        break;
      }
    }
  }
  catch ( boost::system::system_error& boost_error ) {
    // error
    Disconnect();
    return ResultTable::ERROR_DEVICE_INVALID_STOP_BITS;
  }

  // set parity
  try {
    switch( parity_ ) {
      case NONE: {
        boost::asio::serial_port_base::parity
          parity( boost::asio::serial_port_base::parity::none );

        ( *serial_port_ ).set_option( parity );
        break;
      }
      case ODD: {
        boost::asio::serial_port_base::parity
          parity( boost::asio::serial_port_base::parity::odd );

        ( *serial_port_ ).set_option( parity );
        break;
      }

      case EVEN: {
        boost::asio::serial_port_base::parity
          parity( boost::asio::serial_port_base::parity::even );

        ( *serial_port_ ).set_option( parity );
        break;
      }
    }
  }
  catch ( boost::system::system_error& boost_error ) {
    // error
    Disconnect();
    return ResultTable::ERROR_DEVICE_INVALID_PARITY;
  }

  // reset error counter
  error_counter_ = 0;

  return ResultTableManager(SUCCESS);
}

void Manager::Disconnect( void ) {
  // check if is connected
  if( true == is_connected_ ) {

    // close connection
    ( *serial_port_ ).close();

    // reset variables
    is_connected_ = false;
  }

  // reset memory
  if( true == is_mem_allocated_ ) {
    // reset smart pointers
    timer_timeout_.reset();
    serial_port_.reset();
    io_service_.reset();

    is_mem_allocated_ = false;
  }

}

const ResultTable Manager::TransmitData( const unsigned char tx_data_buffer[],
                                         const unsigned int tx_data_size ) {
  // check if device is connected
  if( false == is_connected_ ) {
    return ResultTableManager(ERROR_NOT_CONNECTED);
  }

  // try to transmit data
  try {
    ( *serial_port_ ).write_some(
        boost::asio::buffer( tx_data_buffer, tx_data_size ) );
  }
  catch( boost::system::system_error& boost_error ) {
    return ResultTableManager(ERROR_TX);
  }

  return ResultTableManager(SUCCESS);
}

const ResultTable Manager::ReceiveData(
    std::vector<unsigned char>& rx_data_buffer ) {

  // check if device is connected
  if( false == is_connected_ ) {
    return ResultTableManager(ERROR_NOT_CONNECTED);
  }

  // clear buffer
  if( 0 < rx_data_buffer.size() ) {
    rx_data_buffer.clear();
  }


  // get maximum data buffer size
  const size_t maximum_size =
      ( size_t )( DefinitionTable::MAXIMUM_DATA_BUFFER_SIZE );

  while ( true ) {

    // check if maximum buffer size is reached
    if ( maximum_size <= rx_data_buffer.size() ) {
      return ResultTableManager(ERROR_MAXIMUM_BUFFER_SIZE);
    }

    // buffers byte
    unsigned char rx_data = 0;

    // receive byte
    const ResultTable result = ReceiveByte( rx_data );

    // receive data
    switch ( result ) {
      case SUCCESS: {

        // push new byte to end of the vector
        rx_data_buffer.push_back( rx_data );

        // check if escape sequence is received
        if( rx_data_buffer.size() >= 2) {
          // check if escape sequence was received
          const unsigned int pos = rx_data_buffer.size() - 1;

          if( rx_data_buffer[ pos - 1 ] == '\r' &&
              rx_data_buffer[ pos ] == '\n') {

            // delete last two entrys
            rx_data_buffer.pop_back();
            rx_data_buffer.pop_back();

            // receiving is finished
            return SUCCESS;
          }
        }

        break;
      }
      case ERROR_RX: {
        return ResultTableManager(ERROR_RX);
        break;
      }
      case ERROR_RX_TIMEOUT: {
        return ResultTableManager(ERROR_RX_TIMEOUT);
        break;
      }
      default: {
        return ResultTableManager(ERROR);
        break;
      }
    }
  }

  return ResultTableManager(SUCCESS);
}

const std::string& Manager::get_device_path( void ) {
  return device_path_;
}

const BaudTable Manager::get_baud( void ) {
  return baud_;
}

const DataBitsTable Manager::get_data_bits( void ) {
  return data_bits_;
}

const ParityTable Manager::get_parity( void ) {
  return parity_;
}

const StopBitTable Manager::get_stop_bits( void ) {
  return stop_bits_;
}

const double Manager::get_timeout_max( void ) {
  return timeout_max_;
}

const unsigned int Manager::get_error_counter( void ) {
  return error_counter_;
}

const bool Manager::is_connected( void ) {
  return is_connected_;
}

const ResultTable Manager::ReceiveByte( unsigned char& rx_data ) {

  unsigned char data_buffer[1] = {0};
  bool          data_available = false;
  bool          rx_cancelled = false;

  // read data async with boost
  ( *serial_port_ ).async_read_some
      ( boost::asio::buffer( data_buffer ),
        boost::bind( &Manager::ReadCallback,
        this,
        boost::ref( data_available ),
        boost::ref( *timer_timeout_ ),
        boost::asio::placeholders::error,
        boost::asio::placeholders::bytes_transferred )
       );

  // setup timeout timer
  boost::posix_time::millisec timeout_time( timeout_max_ );

  ( *timer_timeout_ ).expires_from_now( timeout_time );
  ( *timer_timeout_ ).async_wait( boost::bind( &Manager::WaitCallback,
                                  this,
                                  boost::ref( *serial_port_ ),
                                  boost::asio::placeholders::error,
                                  boost::ref( rx_cancelled ))
                                );

  // block program until RX is completed or failed
  ( *io_service_ ).run();

  // reset
  ( *io_service_ ).reset();

  // check if rx was cancelled due to error
  if( rx_cancelled ) {
   return ERROR_RX;
  }
  // check if data was received successfull
 else if( !data_available ) {
    //no data was received -> timeout
    return ERROR_RX_TIMEOUT;
  }

  // write data to output
  rx_data = data_buffer[0];

  return ResultTable::SUCCESS;
}

void Manager::ReadCallback( bool& data_available,
                            boost::asio::deadline_timer& timer_timeout,
                            const boost::system::error_code& boost_error_code,
                            std::size_t num_bytes_transferred ) {

  //check if error occured or no data has been transferred
  if ( ( boost_error_code ) ||
       ( !num_bytes_transferred ) ) {
    data_available = false;
    return;
  }

  // data was received successfull
  // cancel ( stop ) timeout timer to prevent timeout error
  timer_timeout.cancel();
  data_available = true;
}

void Manager::WaitCallback( boost::asio::serial_port& serial_port,
                            const boost::system::error_code& boost_error_code,
                            bool& rx_cancelled ) {
  // check if boost error was received
  if ( boost_error_code ) {
    //no error detected
    rx_cancelled = false;
    return;
  }

  // cancel receiving
  rx_cancelled = true;
  serial_port.cancel();
}

const ResultTable Manager::ResultTableManager(const ResultTable& result) {
  // check for success or not connected
  if( result == SUCCESS || result == ERROR_NOT_CONNECTED ) {
    // reset error counter
    error_counter_ = 0;
  }
  // check for error
  else {
    // increase error counter
    error_counter_++;

    // check if maximum number of errors have been reached
    if( MAXIMUM_ERROR_DISCONNECT <= error_counter_) {
      // to many errors
      Disconnect( );
    }
  }

  return result;
}

};
