/*
Copyright (c) 2016, Martin Fees (martin.fees@gmx.de)
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 * Neither the name of  nor the names of its contributors may be used to
   endorse or promote products derived from this software without specific
   prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
*/

/**
 * @file serial_interace_test.cc
 *
 * @brief Application for testing serial interface
 *
 * @author    Martin Fees
 * @date      20.08.2016
 * @copyright BSD 3-Clause
 *
 * This program tests the functions of the serialinterface namespace.
 */

#include <ros_serial_can_interface/serial_interface.h>

/**
 * @brief Function to test serialinterface::Manager::Connect( ) function
 *
 * @param device_path[in] Path to the device
 *
 * @return see serialinterface::ResultTable
 */
void serialinterface_Manager_Connect(const std::string& device_path) {
  //variables
  std::vector<unsigned char> rx_data_buffer;
  serialinterface::ResultTable result = serialinterface::SUCCESS;


  //creating interface
  serialinterface::Manager  serial_manager
                              ( device_path, serialinterface::BAUD_9600,
                                serialinterface::DATA_8, serialinterface::NONE,
                                serialinterface::STOP_1, 5000 );

  //check if error is throwed when transmitData is called before connect
  unsigned char tx_data[] = {1, 3, 4};
  unsigned char tx_data_size = 3;

  std::cout << "Transmit data (expect NOT CONNECTED)\t: ";
  if(serialinterface::ERROR_NOT_CONNECTED ==
      serial_manager.TransmitData(tx_data, tx_data_size)) {
      std::cout << "NOT_CONNECTED (pass)" << std::endl;
  }
  else {
    std::cout << " != NOT_CONNECTED (fail)!" << std::endl;
  }

  //check if RX failed
  std::cout << "Receive data (expect NOT CONNECTED)\t: ";
  if(serialinterface::ERROR_NOT_CONNECTED ==
      serial_manager.ReceiveData(rx_data_buffer)) {
      std::cout << "NOT_CONNECTED (pass)" << std::endl;
  }
  else {
    std::cout << " != NOT_CONNECTED (fail)!" << std::endl;
  }

  //connect
  result = serial_manager.Connect();

  //check result
    switch(result) {
      case serialinterface::SUCCESS: {
        std::cout << "Device " << device_path.c_str()
            << " connected " << std::endl;
        break;
      }
      case serialinterface::ERROR_OPERATION_PERMITTED: {
        std::cout << "error: Device " << device_path.c_str()
            << " opening was permitted!" << std::endl;
        break;
      }
      case serialinterface::ERROR_DEVICE_NOT_FOUND: {
        std::cout << "error: Device " << device_path.c_str()
            << " was not found! Is the path correct?" << std::endl;
        break;
      }
      case serialinterface::ERROR_DEVICE_PERMISSION_DENIED: {
        std::cout << "error: Device " << device_path.c_str()
                  << " permission denied! Check access rights!" << std::endl;
        break;
      }
      case serialinterface::ERROR_DEVICE_INVALID_BAUD: {
        std::cout << "error: Device " << device_path.c_str()
                   << " baud of " << static_cast<int>(serial_manager.get_baud())
                    << " is not supported by device" << std::endl;
        break;
      }
      case serialinterface::ERROR_DEVICE_INVALID_FLOW_CTRL: {
        std::cout << "error: Device " << device_path.c_str()
            << " flow control is not supported!" << std::endl;
        break;
      }
      case serialinterface::ERROR_DEVICE_INVALID_DATA_BITS: {
        std::cout << "error: Device " << device_path.c_str()
            << " data bits are not supported!" << std::endl;
        break;
      }
      case serialinterface::ERROR_DEVICE_INVALID_STOP_BITS: {
        std::cout << "error: Device " << device_path.c_str()
            << " stop bits are not supported!" << std::endl;
        break;
      }
      case serialinterface::ERROR_DEVICE_INVALID_PARITY: {
        std::cout << "error: Device " << device_path.c_str()
                    << " parity setting is not supported!" << std::endl;
        break;
      }
      default: {
        std::cout << "error: Device " << device_path.c_str()
            << " error opening device" << std::endl;
        break;
      }
    }

    // send package
    std::cout << "Sending data packet\t: ";
    result = serial_manager.TransmitData(tx_data, tx_data_size);


    switch ( result ) {
      case serialinterface::SUCCESS: {
        std::cout << " SUCCESS" << std::endl;
        break;
      }
      case serialinterface::ERROR_NOT_CONNECTED: {
        std::cout << " FAIL: Not connected!" << std::endl;
        break;
      }
      case serialinterface::ERROR_TX: {
        std::cout << " FAIL: Transmit error!" << std::endl;
        break;
      }
      case serialinterface::ERROR: {
          std::cout << " FAIL: general error" << std::endl;
        break;
      }
      default: {
        std::cout << " FAIL: Uknown error " << std::endl;
      }
    }

    //receiving data packet
    std::cout << "Receiving data packet\t: ";
    result = serial_manager.ReceiveData( rx_data_buffer );


    switch ( result ) {
      case serialinterface::SUCCESS: {
        std::cout << " SUCCESS" << std::endl;
        break;
      }
      case serialinterface::ERROR_NOT_CONNECTED: {
        std::cout << " FAIL: Not connected!" << std::endl;
        break;
      }
      case serialinterface::ERROR_RX: {
        std::cout << " FAIL: receive error" << std::endl;
        break;
      }
      case serialinterface::ERROR_RX_TIMEOUT: {
        std::cout << " FAIL: Timeout" << std::endl;

        break;
      }
      case serialinterface::ERROR_MAXIMUM_BUFFER_SIZE: {
        std::cout << " FAIL: Maximum buffer size reached" << std::endl;
        break;
      }
      case serialinterface::ERROR: {
        std::cout << " FAIL: general error" << std::endl;
        break;
      }
      default: {
        std::cout << " FAIL: Unknown error " << std::endl;
      }
    }



  //disconnect
  serial_manager.Disconnect();
}

/**
 * @function main
 *
 * @brief Main function for testing correct implementation
 *
 */
int main( int argc, char** argv )
{
  serialinterface_Manager_Connect("/dev/ttyACM0");


  return 0;
}
