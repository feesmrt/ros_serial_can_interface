# ros_serial_can_interface

This package is a cheap solution to establish a communication between
ROS and a CAN network.
It was developed during my master thesis:" Development of a robotarm
for inspection and manipulation tasks for mobile rescue robotics"
(translated from the german original title: "Entwicklung eines Roboterarms für
Inspektions-und Manipulationsaufgaben in der Rettungsrobotik") at the
[Nuremberg Institute of Technology](http://www.th-nuernberg.eu/home/page.html)
for controlling the actuators of a new developed robotarm with ROS.
The robotarm is used by the team [AutonOHM](http://www.autonohm.de/)
for a mobile rescue robot.

The package was successfully tested in a CAN network with four nodes
(4x motor controllers Elmo MC Gold DC Whistle).


## Hardware

An [Arduino UNO](https://www.arduino.cc/en/Main/ArduinoBoardUno) with the
[Watterott CANdiy-Shield](http://www.watterott.com/de/Arduino-CANdiy-Shield)
is used as hardware.

![Arduino UNO with CANdiy-Shield](https://raw.githubusercontent.com/watterott/CANdiy-Shield/master/hardware/CANdiy-Shield_v13.jpg)

*Arduino UNO with CANdiy-Shield
(https://raw.githubusercontent.com/watterott/CANdiy-Shield/master/hardware/CANdiy-Shield_v13.jpg)*

The CAN nodes are connected with a RJ45 cable.
The CAN communication is controlled by a
[Microchip MCP2515 CAN Controller](http://ww1.microchip.com/downloads/en/DeviceDoc/21801G.pdf)
connected via SPI to the Arduino uC.
The Arduino UNO is connected via USB to the host PC.

![Overview of hardware](https://gitlab.com/feesmrt/ros_serial_can_interface/blob/bf637fcbabad3f6d96f08985e7bc71c131d28aff/doc/img/ros_arduino_can_overview.png)

## Software

There are two parts of the software:

1. Arduino UNO Firmare: Converting data Serial <-> CAN and managing MCP2515 chip
2. ROS node: Accessing VCP via boost and sending/receiving CAN messages via can.msg file

###### Arduino UNO Firmware

The firmware can be compiled and flashed to the Arduino UNO with the
[Arduino IDE](https://www.arduino.cc/en/Main/Software) Software.
To control the MCP2515 Chip the software of Seeed-Studio is used,
which is found at this [repository](https://github.com/Seeed-Studio/CAN_BUS_Shield).
Besides the part of the firmware which controls the CAN controller there is a
part which manages the communication via VCP.
The communication is fully managed by the ROS node running on the host PC.
The ROS node sends commands to the Arduino and wait for the response.
There are two commands:
The first command sends data to the CAN network and the second asks for the
received data in the buffer of the MCP2515 chip.
All messages send via the VCP have a major format shown in the table below:

| Byte         | 1       | 2 - 11   | 12 - 13         |
| :----------: | :-----: | :------: | :-------------: |
| Description  | Command | Content  | Escape sequence |
*Major structure of a serial message send via VCP*

If the Host-PC wants to send a message to the CAN network the message contains
the CAN-ID (2 bytes) and the CAN-DATA (up to 8 bytes).

| Byte         | 1       | 2  - 3    | 4 - (11) |  (12) - 13      |
| :----------: | :-----: | :-------: | :------: | :-------------: |
| Description  | Command | CAN-ID    | CAN-DATA | Escape sequence |
| Value        | 0x11    | [...]     | [...]    | 0x0D 0x0A       |
*Message from Host-PC to Arduino to send data to the CAN network*

The second command is used to ask for received CAN messages stored in the
data buffer of the MCP2515 Chip.

| Byte         | 1       |  2 - 3          |
| :----------: | :-----: | :-------------: |
| Description  | Command | Escape sequence |
| Value        | 0x44    | 0x0D 0x0A       |
*Message with command for reading the CAN message buffer*

There are two answers which can be send from the Arduino to the Host-PC:
If there is now message in the RX-Buffer of the MCP2515 Chip the
Arduino sends the message shown below:

| Byte         | 1       |  2 - 3          |
| :----------: | :-----: | :-------------: |
| Description  | Command | Escape sequence |
| Value        | 0xFF    | 0x0D 0x0A       |
*Response if RX message buffer is empty*

In the case the buffer contains data, the answer of the Arduino is filled
with the CAN message:

| Byte         |  1 - 2  | 3 - (10) |  (11) - 12      |
| :----------: | :----:  | :------: | :-------------: |
| Description  | CAN-ID  | CAN-DATA | Escape sequence |
| Value        | [...]   | [...]    | 0x0D 0x0A       |
*Respone if RX message contains a CAN message*

The communication speed of the VCP is configured by the host PC,
when the connection is established.
To set the communication speed of the CAN network the definition has to be
adjusted in the firmware file of the Arduino UNO:

```
#include <SPI.h>
#include "mcp_can.h"
#include "mcp_can_dfs.h"

/**
 * @brief Definitions for CAN Interface
 */
 #define CAN_BAUD_RATE      CAN_1000KBPS // Communication speed at CAN network
 #define CAN_LED_1          7
 #define CAN_LED_2          8
 #define CAN_INTERRUPT      2
 #define CAN_CS             10
 #define CAN_BUFFER_SIZE    8
```
*The communication speed is by default set to 1 MB/s*

**Bandwith-Limitation:** The maximum bandwith of the VCP is current 115,2 kB/s.
With the overhead of the messages the communication speed is even lower.
You've to consider this limitation when using the Arduino as CAN interface.
The firmware throws received messages away if the buffer is full.

###### ROS node

The ROS node for controlling the Arduino is located in the package
**ros_serial_can_interface**. The name of the node is the same as the
package name.
You can use the launch file to start the node:

```
roslaunch ros_serial_can_interface serial_can_interface.launch
```

You've a few parameters which can be used to config your interface:

```
<launch>
	<node name="ROSSerialCANInterface"
	  pkg="ros_serial_can_interface"
	  type="ros_serial_can_interface"
	  output="screen">
	  <param name="baud"              value="115200"       type="int"    />
	  <param name="serialDevicePath"  value="/dev/ttyACM0" type="string" />
	  <param name="updateRateHz"      value="500.0"        type="double" />
	  <param name="timeout"           value="100"          type="int"    />
	</node>
</launch>
```

1. **baud**: Setup the baud rate between host PC and Arduino UNO

2. **serialDevicePath**: Path to your Arduino UNO. Use *ls /dev/* to check where
   your Arduino is located.

3. **updateRateHz**: Update rate of the ROS node. 
This is maybe a little bit confusing. This rate determines the speed of 
getting received CAN messages from
Arduino. A CAN message is sent immidiatly to the Arduino when the node 
receives it. The update rate unit is hertz.

4. **timeout**: Maximum time to wait for an answer from the Arduino before a
error is throwed. The time unit is milliseconds.

# Code Documentation

A complete documentation of the source code generated with doxygen can
be found in this repository at: **/doc/doxygen/html/index.html**
