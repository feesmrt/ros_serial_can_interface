ROS Package ros_serial_can_interface
=================================

Documentation for the software used to establish a ROS (Robot Operating System)
interface for CAN (Controller Area Networks).<br />
The interface is based on an Arduino UNO with a CANdiy shield from Watterott.<br />
Please go to the gitlab repository
https://gitlab.com/feesmrt/ros_serial_can_interface for the general
documentation.
