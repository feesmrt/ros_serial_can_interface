var indexSectionsWithContent =
{
  0: "bcdegimnoprst~",
  1: "mr",
  2: "s",
  3: "crs",
  4: "cdgimrt~",
  5: "bdprs",
  6: "bdemnos",
  7: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "enums",
  6: "enumvalues",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Enumerations",
  6: "Enumerator",
  7: "Pages"
};

