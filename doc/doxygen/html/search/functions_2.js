var searchData=
[
  ['get_5fbaud',['get_baud',['../classserialinterface_1_1Manager.html#acaed54df596dd9cb937ddb1d479c2916',1,'serialinterface::Manager']]],
  ['get_5fdata_5fbits',['get_data_bits',['../classserialinterface_1_1Manager.html#aa3a537cefa1c09613bce2f4d28c3149d',1,'serialinterface::Manager']]],
  ['get_5fdevice_5fpath',['get_device_path',['../classserialinterface_1_1Manager.html#a667f1310851d0915a8af809d45fa765e',1,'serialinterface::Manager']]],
  ['get_5ferror_5fcounter',['get_error_counter',['../classserialinterface_1_1Manager.html#acbeec4d2b58b514ea54c9731c6e22f3b',1,'serialinterface::Manager']]],
  ['get_5fparity',['get_parity',['../classserialinterface_1_1Manager.html#a4f8cebe053c772b3b3d7649753680b9b',1,'serialinterface::Manager']]],
  ['get_5fstop_5fbits',['get_stop_bits',['../classserialinterface_1_1Manager.html#a6b807fd715c3213e255091a548886aa9',1,'serialinterface::Manager']]],
  ['get_5ftimeout_5fmax',['get_timeout_max',['../classserialinterface_1_1Manager.html#a84a6fa2afde66052815473cf1f6c44e3',1,'serialinterface::Manager']]]
];
